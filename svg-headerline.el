;;; package --- Summary
;;; Commentary:
;;; This is a package that makes a header line using svg-lib
;;; Warning, bring a fork because the below code is absolute spaghetti right now
;;; I will clean it up later once I learn mode about header-line and elisp.
;;; For now though, this is a bit rough (read really rough)

;;; To use, just call the function svg-headerline or load this file and add (svg-headerline) to your init file
;;; Code:
(require 'svg-lib)
;;; :TODO: Figure out lsp-mode and how to get the arrow only when which-function-mode is enabled
(which-function-mode 1)
;;; Keeping this here so I can easily edit it and run it without having to call the function
;;; It's the same code as in the function below

(setq-default header-line-format
              '((:eval
                 (let* ((persp-names (persp-names))
                        (persp-str (mapconcat (lambda (name)
                                                (if (equal name (persp-current-name))
                                                    (propertize name 'face '(:weight bold))
                                                  name))
                                              persp-names
                                              "|"))
                        (mode-str (propertize (format-mode-line mode-name) 'face '(:weight bold)))
                        (date-str (propertize (format-time-string "%b %d %H:%M") 'face '(:weight bold)))
                        (header-length (length (concat "[" persp-str "]")))
                        (svg-buffer-path
                         (concat (file-name-nondirectory (directory-file-name (file-name-directory default-directory)))
                                 "➲"
                                 (file-relative-name buffer-file-name default-directory)))
                        (path-str (if (buffer-modified-p)
                                      (concat (propertize svg-buffer-path 'face '(:weight bold)) "*")
                                    (propertize svg-buffer-path 'face '(:weight bold))))
                        (meow-str (if (fboundp 'meow-indicator)
                                      (concat (propertize " " 'display '(meow-insert-indicator))
                                              (meow-indicator))
                                    (format-mode-line mode-name)))
                        (func-str (if (bound-and-true-p which-function-mode)
                                      (propertize (format-mode-line which-func-format) 'face '(:weight bold))
                                    "NA"))
                        (my/left-str (concat "▌" meow-str " " path-str " ➲ " func-str))
			(my/left-str-width (- (length my/left-str) 2))
                        (my/right-str (concat "[" persp-str "] " mode-str " " date-str))
			(my/right-str-width (- (length my/right-str) 2))
			(my/total-width (+ my/left-str-width my/right-str-width))
			(final-pos (* 2 (-  (window-width) my/total-width)))
			(total-str (concat "▌" meow-str " " path-str (propertize " " 'display `((space :align-to (- right (+ 12 ,header-length ,(length date-str))))))
					   "[" persp-str "] " mode-str " " date-str))
			(value "42"))
                   (propertize my/left-str 'display (svg-image (svg-lib-concat
								(svg-lib-tag my/left-str
									     nil :foreground "#957DAD" :font-weight "bold" :margin 0 :stroke 4 :radius 5 :padding -3)
								(svg-lib-tag my/right-str
									     nil :foreground "#957DAD" :font-weight "bold" :margin final-pos :stroke 3 :radius 5 :padding -2))
							       :ascent 'center))))))









(defun svg-headerline ()
  "Create a mode line using svg-lib"
  (interactive)
  (setq-default header-line-format
		'((:eval
                   (let* ((persp-names (persp-names))
                          (persp-str (mapconcat (lambda (name)
                                                  (if (equal name (persp-current-name))
                                                      (propertize name 'face '(:weight bold))
                                                    name))
						persp-names
						"|"))
			  (func-str (propertize (format-mode-line which-func-format) 'face '(:weight bold)))
                          (mode-str (propertize (format-mode-line mode-name) 'face '(:weight bold)))
                          (date-str (propertize (format-time-string "%b %d %H:%M") 'face '(:weight bold)))
                          (header-length (length (concat "[" persp-str "]")))
                          (svg-buffer-path
                           (concat (file-name-nondirectory (directory-file-name (file-name-directory default-directory)))
                                   "➲"
                                   (file-relative-name buffer-file-name default-directory)))
                          (path-str (if (buffer-modified-p)
					(concat (propertize svg-buffer-path 'face '(:weight bold)) "*")
                                      (propertize svg-buffer-path 'face '(:weight bold))))
                          (meow-str (if (fboundp 'meow-indicator)
					(concat (propertize " " 'display '(meow-insert-indicator))
						(meow-indicator))
                                      (format-mode-line mode-name)))
                          (my/left-str (concat "▌" meow-str " " path-str " ➲ " func-str))
			  (my/left-str-width (- (length my/left-str) 2))
                          (my/right-str (concat "[" persp-str "] " mode-str " " date-str))
			  (my/right-str-width (- (length my/right-str) 2))
			  (my/total-width (+ my/left-str-width my/right-str-width))
			  (final-pos (* 2 (-  (window-width) my/total-width)))
			  (total-str (concat "▌" meow-str " " path-str (propertize " " 'display `((space :align-to (- right (+ 12 ,header-length ,(length date-str))))))
					     "[" persp-str "] " mode-str " " date-str))
			  (value "42"))
                     (propertize my/left-str 'display (svg-image (svg-lib-concat
								  (svg-lib-tag my/left-str
									       nil :foreground "#957DAD" :font-weight "bold" :margin 0 :stroke 4 :radius 5 :padding -3)
								  (svg-lib-tag my/right-str
									       nil :foreground "#957DAD" :font-weight "bold" :margin final-pos :stroke 3 :radius 5 :padding -2))
								 :ascent 'center))))))




  (provide 'svg-headerline)
;;; svg-modeline.el ends here

